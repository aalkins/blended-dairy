/********************************************************************************************************************************

                                              SURESHOT SOLUTIONS
                             Stepper driver for Blended Dairy - Dual TCS Pumps
                                                Version 1.1
                                              by Anthony Alkins 
            
      DESCRIPTION: Wifi enabled and controlled using Blynk App. Can run 2 stepper drivers to enable blended dairy.
                   Takes variables 3 variables from Blynk App:
                   -Size
                   -MilkFatPercent
                   -Dispense                       
                                                          
   ********************************************************************************************************************************/



#include <BlynkSimpleEsp8266.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
//#include "CallbackFunction.h"
#include <AccelStepper.h>


#define DEBUG 0
#define STEPSPERMLCREAM 400.0
#define STEPSPERMLSKIM 400.0
#define OPERATINGSPEED 4500.0
#define ACCELERATION 8000.0

//Define Driver Pins
const byte ENABLE = D5;
const byte STEP1 = D1;
const byte DIR1 = D2;
const byte STEP2 = D3;
const byte DIR2 = D4;

//Define Steppers
AccelStepper stepperCream(AccelStepper::DRIVER,D1,D2);
AccelStepper stepperSkim(AccelStepper::DRIVER,D3,D4);

//Define Variables
int Size = 1;
int MilkFatPercent= 0;
int Dispense = 0;
float SecondarySpeed;
int StepsCream;
int StepsSkim;

unsigned long timeSinceOff = 0;
unsigned long currentTime;

float Volume;
float MilkFat;
float SkimVol;
float CreamVol;
float CreamMilkFat =1.824603*0.18*0.18*0.18-1.067820*0.18*0.18+1.059016*0.18;

const int ledPin = BUILTIN_LED;

boolean motorStatusCurrent;
boolean motorStatus = 0;
boolean previousMotorStatus = 1;

// Some UDP / WeMo specific variables:
String serial; // Where we save the string of the UUID
String persistent_uuid; // Where we save some socket info with the UUID

//Blynk specific Details
char auth[] = "32bb5cc0803241d3a26afbacd448a2d7";

BLYNK_WRITE(V0)  
{
  Size = param.asInt();
  if(DEBUG == 1){
  Serial.println(Size);
  }
}

BLYNK_WRITE(V1)  
{
  MilkFatPercent = param.asInt();
  Blynk.virtualWrite(V5,MilkFatPercent);
  if(DEBUG == 1){
  Serial.println(MilkFatPercent);
  }
}

BLYNK_WRITE(V2)  
{
  Dispense = param.asInt();
  if(DEBUG == 1){
  Serial.println(Dispense);
  }
}


void setup() {

  Serial.begin(115200);
  pinMode(ledPin, OUTPUT); // initialize digital ledPin as an output.
  delay(10);
  digitalWrite(ledPin, HIGH); // Wemos BUILTIN_LED is active Low, so high is off
  
  // Set the UUIDs and socket information:
  prepareIds();

  // Get settings from WiFi Manager:
  WiFiManager wifiManager;
  // wifiManager.resetSettings(); // Uncomment this to test WiFi Manager function
  wifiManager.setAPCallback(configModeCallback);
  wifiManager.autoConnect("WemosD1");

  // Wait til WiFi is connected properly:
  int counter = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    counter++;
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);

  Blynk.begin(auth, WiFi.SSID().c_str(), WiFi.psk().c_str());
  Blynk.syncAll();

  MotorSetup();
}

void loop() {
  if(motorStatus == 0){
  Blynk.run();
  }
  check4Dispense();
  pumpStatus();
  if(DEBUG == 1){
    Serial.print(motorStatusCurrent);
    Serial.print(motorStatus);
    Serial.println(previousMotorStatus);
  }
}

void check4Dispense(){
  if(Dispense == 1){
    dispenseRecipe();
    //do the motor stuff

    RunPumps();

    //motor stuff done

    if(DEBUG == 1 || DEBUG == 2){
      Serial.print("Dispensing a ");
      Serial.print(MilkFatPercent);
      Serial.print("% ");
      if(Size == 1){
        Serial.print("Small \n");
      }
      if(Size == 2){
        Serial.print("Medium \n");
      }
      if(Size == 3){
        Serial.print("Large \n");
      }
    }
    //delay(1500);

    if (DEBUG == 1 || DEBUG==2){
    Serial.println("***********************************************");
    }
  }
}

void dispenseRecipe(){
  float MilkFatPercent1 = float(MilkFatPercent)/100.0;
  if(Size == 1){
    Volume = 25.0; //dispense size is 25ml
    MilkFat = 1.824603*MilkFatPercent1*MilkFatPercent1*MilkFatPercent1-1.067820*MilkFatPercent1*MilkFatPercent1+1.059016*MilkFatPercent1;
    CreamVol = Volume*MilkFat/(CreamMilkFat);
    SkimVol = Volume - CreamVol;
    if(DEBUG == 1 || DEBUG == 2){
     Serial.print("Cream Volume = ");
     Serial.print(CreamVol);
     Serial.println(" ml" );
     Serial.print("Skim Volume = ");
     Serial.print(SkimVol);
     Serial.println(" ml" );
    }
  }
  if(Size == 2){
    Volume = 35.0; //dispense size is 35ml
    MilkFat = 1.824603*MilkFatPercent1*MilkFatPercent1*MilkFatPercent1-1.067820*MilkFatPercent1*MilkFatPercent1+1.059016*MilkFatPercent1;
    CreamVol = Volume*MilkFat/(CreamMilkFat);
    SkimVol = Volume - CreamVol;
    if(DEBUG == 1 || DEBUG == 2){
     Serial.print("Cream Volume = ");
     Serial.print(CreamVol);
     Serial.println(" ml" );
     Serial.print("Skim Volume = ");
     Serial.print(SkimVol);
     Serial.println(" ml" );
    }
  }
  if(Size == 3){
    Volume = 50.0; //dispense size is 50ml
    MilkFat = 1.824603*MilkFatPercent1*MilkFatPercent1*MilkFatPercent1-1.067820*MilkFatPercent1*MilkFatPercent1+1.059016*MilkFatPercent1;
    CreamVol = Volume*MilkFat/(CreamMilkFat);
    SkimVol = Volume - CreamVol;
    if(DEBUG == 1 || DEBUG == 2){
     Serial.print("Cream Volume = ");
     Serial.print(CreamVol);
     Serial.println(" ml" );
     Serial.print("Skim Volume = ");
     Serial.print(SkimVol);
     Serial.println(" ml" );
    }
  }
  Blynk.virtualWrite(V3,CreamVol);
  Blynk.virtualWrite(V4,SkimVol);
}

void prepareIds() {
  uint32_t chipId = ESP.getChipId();
  char uuid[64];
  sprintf_P(uuid, PSTR("38323636-4558-4dda-9188-cda0e6%02x%02x%02x"),
  (uint16_t) ((chipId >> 16) & 0xff),
  (uint16_t) ((chipId >>  8) & 0xff),
  (uint16_t)   chipId        & 0xff);

  serial = String(uuid);
  persistent_uuid = "Socket-1_0-" + serial;
}



void configModeCallback(WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println("Soft AP's IP Address:");
  Serial.println(WiFi.softAPIP());
  Serial.println("WiFi Manager: Please connect to AP:");
  Serial.println(myWiFiManager->getConfigPortalSSID());
  Serial.println("To setup WiFi Configuration");
}

void MotorSetup(){
    pinMode(ENABLE, OUTPUT);
//    pinMode(STEP1, OUTPUT);
//    pinMode(DIR1, OUTPUT);
//    pinMode(STEP2, OUTPUT);
//    pinMode(DIR2, OUTPUT);
 
  //turn off current to motor
    digitalWrite(ENABLE, HIGH);
 
 
  //SET CURRENT LIMIT (DIGITAL CURRENT CONTROL)
//Note: No need to change this when using potentiometer control
 /* I1   I2  Current Limit
     Z   Z      0.5 A
     Low Z      1 A
     Z   Low    1.5 A
     Low Low    2 A        */
// set Digital current limit to 2A
//    pinMode(I1, OUTPUT); digitalWrite(I1, LOW);   /* DISABLE THIS LINE FOR HIGH IMPEDANCE (Z) ON I1 */
//    pinMode(I2, OUTPUT); digitalWrite(12, LOW);   /* DISABLE THIS LINE FOR HIGH IMPEDANCE (Z) ON I2 */
      
// SET MICROSTEP MODE
//    pinMode(MS1, OUTPUT);
//    pinMode(MS2, OUTPUT);
/*   MS1   MS2    Microstep Resolution
     Low   Low      Full step
     High  Low      Half (1/2) step
     Low   High     Quarter (1/4) step
     High  High     Eighth (1/8) step    */
// set step mode to 1/4 STEP
//    digitalWrite(MS1, LOW); 
//    digitalWrite(MS2, HIGH);

//Speed is in steps/s
//Accel is in steps/s^2

    stepperCream.setAcceleration(ACCELERATION);
    stepperSkim.setAcceleration(ACCELERATION);
    
}

void RunPumps(){
  //turn ON current to motor
  digitalWrite(ENABLE, LOW);
  timeSinceOff = 0;
  
  //if 400 steps = 1 ml --> stored in STEPSPERML variables
  StepsCream = CreamVol*STEPSPERMLCREAM;
  StepsSkim = SkimVol*STEPSPERMLSKIM;
  if (DEBUG==2){
    Serial.println(StepsCream);
    Serial.println(StepsSkim);
  }
  
  if(StepsCream > StepsSkim){
    int DispenseTime = (StepsCream/OPERATINGSPEED);
    stepperCream.setMaxSpeed(OPERATINGSPEED);
    SecondarySpeed = StepsSkim/(DispenseTime+1);   //Bug with if cream dispense is larger it runs longer than the SKIM dispense
    stepperSkim.setMaxSpeed(SecondarySpeed);
    if (DEBUG==2){
      Serial.println("Cream Larger");
    }
  }
  else{
    int DispenseTime = StepsSkim/OPERATINGSPEED;
    stepperSkim.setMaxSpeed(OPERATINGSPEED);
    SecondarySpeed = StepsCream/(DispenseTime+1);
    stepperCream.setMaxSpeed(SecondarySpeed);
    if (DEBUG==2){
      Serial.println("Skim Larger");
    }
  }
  if (DEBUG==2){
  Serial.println(SecondarySpeed);
  }

  stepperCream.move(StepsCream);
  stepperSkim.move(StepsSkim);
}

void pumpStatus(){

  if(StepsCream > 0){
  motorStatusCurrent = stepperCream.isRunning();
  }
  else{
  motorStatusCurrent = stepperSkim.isRunning();
  }
  Dispense = 0;
 
  stepperCream.run();
  stepperSkim.run();

  if(motorStatusCurrent != previousMotorStatus){
    previousMotorStatus = motorStatusCurrent;
  }
  if(motorStatus != previousMotorStatus){
    motorStatus = previousMotorStatus;
    if(motorStatus == 0){
        //turn of toggle switch in app
        Blynk.virtualWrite(V2,0); 

    }
  }
  if(motorStatusCurrent == 0 && timeSinceOff == 0){
    timeSinceOff = millis();
  }
    if(timeSinceOff>0){
      currentTime = millis();
      if(currentTime - timeSinceOff > 4000){
        //turn OFF current to motor
        digitalWrite(ENABLE, HIGH);
        timeSinceOff = 0;
      }
    }
  }


